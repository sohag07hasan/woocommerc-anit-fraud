<?php
/*
 * Handles the databass 
 */

class WooAntiFraudDb{
	
	private $customer_table;
	private $customermeta_table;
	private $blacklist_table;
	private $db;
		
	//constructor
	function __construct(){
		global $wpdb;
		$this->customer_table = $wpdb->prefix . 'woo_customers';			
		$this->customermeta_table = $wpdb->prefix . 'woo_customer_meta';
		$this->blacklist_table = $wpdb->prefix . 'woo_blacklist';
		$this->db = $wpdb;
	}
	
	
	//This will create the db talbe
	function sync_db(){
		global $wpdb;
		$sql = array();
		$sql[] = "create table if not exists $this->customer_table(
			ID bigint not null auto_increment primary key,
			username varchar(100) not null,
			email varchar(100) not null unique,
			status tinyint not null default 1
		)";
		
		$sql[] = "create table if not exists $this->customermeta_table(
			meta_id bigint not null auto_increment primary key,
			customer_id bigint not null,
			meta_key text not null,
			meta_value text,
			status tinyint not null default 1
		)";
		
		$sql[] = "create table if not exists $this->blacklist_table(
			id bigint not null auto_increment primary key,
			type varchar(100) not null,
			value text not null,
			status tinyint not null default 1
		)";
		
		
		//var_dump($sql); exit;
		
		foreach($sql as $s){
			$wpdb->query($s);
		}
	}
	
	
	/**
	 * retursn a customer
	 * @type = key (username, email etc) to search
	 * @value = value to search
	 * */
	function get_customer_by($type, $value){
		$sql = sprintf("select * from $this->customer_table where %s like '%s'", $type, $value);
		return $this->db->get_row($sql);
	}
	
	
	/**
	 * get the customer meta from customer_meta table
	 * @id = customer id (required)
	 * @meta_key = required
	 * */
	function get_customer_meta($id, $meta_key){
		$sql = $this->db->prepare("select meta_value from $this->customermeta_table where customer_id like '%s' and meta_key like '%s'", $id, $meta_key);
		return $this->db->get_var($sql);		
	}
	
	
	
	/**
	 * insert or update a customer based
	 * */
	function insert_customer($info){
		$existing_customer = $this->get_customer_by('email', $info['email']);
		
		//var_dump($existing_customer);
		
		$is_update = false;
		
		if($existing_customer){
			$is_update = true;
			$info['ID'] = (int) $existing_customer->ID;
		}
		
		if($is_update){
			$this->db->update($this->customer_table, array('username' => $info['username'], 'status' => (int)$info['status']), array('ID' => $info['ID']), array('%s', '%d'), array('%d'));
		}
		else{
			$this->db->insert($this->customer_table, array('username' => $info['username'], 'email' => $info['email'], 'status' => (int)$info['status']), array('%s', '%s', '%d'));
			$info['ID'] = $this->db->insert_id;
		}
		
		return $info['ID'];
	}
	
	
	/**
	 * customer meta data
	 * @customer_id required
	 * @key required
	 * @value may be null 
	 * */
	function update_customer_meta($customer_id, $key, $value){
		if($this->db->get_var($this->db->prepare("select meta_id from $this->customermeta_table where customer_id like '%s' and meta_key like '%s'", $customer_id, $key))){
			return $this->db->update($this->customermeta_table, array('meta_value' => $value), array('customer_id' => $customer_id, 'meta_key' => $key), array('%s'), array('%d', '%s'));
		}
		else{
			return $this->add_customer_meta($customer_id, $key, $value);
		}
	}
	
	
	/**
	 * adds new row with customer meta
	 * @customer_id required
	 * @key required
	 * @value may be null 
	 * */
	function add_customer_meta($customer_id, $key, $value){
		if($customer_id){
			return $this->db->insert($this->customermeta_table, array('customer_id' => (int)$customer_id, 'meta_key' => $key, 'meta_value' => $value), array('%d', '%s', '%s'));
		}
	}
	
	
	
	/**
	 * Speacial function to add order
	 * @customer_id required
	 * @key required
	 * @value may be null 
	 * */
	function add_order_for_a_customer($customer_id, $key, $value){
		if($meta_id = $this->db->get_var($this->db->prepare("select meta_id from $this->customermeta_table where customer_id like '%s' and meta_key like '%s' and meta_value like '%s'", $customer_id, $key, $value))){
			return $this->db->update($this->customermeta_table, array('meta_value' => $value), array('meta_id' => $meta_id, 'customer_id' => $customer_id, 'meta_key' => $key), array('%s'), array('%d', '%d', '%s'));
		}
		else{
			return $this->add_customer_meta($customer_id, $key, $value);
		}
	}
	
	
	
	/**
	 * Detected frauds are save in fraud database table
	 * */
	function save_detected_frauds($info){
		
	//	var_dump($info); exit;
		
		if(count($info) > 0){
			foreach($info as $type => $value){
				if(!$this->db->get_var($this->db->prepare("select id from $this->blacklist_table where type like '%s' and value like '%s'", $type, $value))){
					$this->db->insert($this->blacklist_table, array('type' => $type, 'value' => $value), array('%s', '%s'));
				}
			}
		}
	}
	
	
	
	/**
	 * Blacklisted data
	 * @type = type of blacklist (username, email, etc)
	 * @value = value of the specific type
	 * */
	function is_black_listed($type, $value){
		$sql = $this->db->prepare("select id from $this->blacklist_table where type like '%s' and value like '%s' and status like '%d'", $type, $value, 1);
		return $this->db->get_var($sql) ? true : false;
	}
	
	
	
	/**
	 * return the blacklisted data
	 * @ type (required) username, email etc
	 * */
	function get_blacklists_by($type){
		$sql = $this->db->prepare("select * from $this->blacklist_table where type like '%s' order by type", $type);
		return $this->db->get_results($sql);
	}
	
	
	
	/***
	 * Save blacklists status 
	 * @by index key for frauds table
	 * @status status key (1, 2)
	 * */
	function update_blacklist_status($by, $value, $status){
		return $this->db->update($this->blacklist_table, array('status'=>$status), array($by=>$value));		
	}
}