<?php
/*
 * Fetches ip address information from http://www.ip-score.com/
 * */

class WooIpInfo{
	
	private $ip = null;
	private $html = null;
	private $url = null;
	private $ip_info = array();
	private $matches = array();
	
	private $info_keys = array(
		0 => 'country',
		1 => 'state',
		2 => 'city',
		6 => 'isp',
		10 => 'time_zone'
	);
	
	//conststs
	const info_holder_url = 'http://www.ip-score.com/';
	
	
	/*
	 *constructor 
	 */
	function __construct($ip = null){
		$this->ip = $ip;
		$this->url = self::info_holder_url . 'checkip/' . $this->ip;
	}
	
	/*
	 * get all info
	 * */
	function get_ip_info(){
		$html = $this->get_html();
		
		if(!empty($html)){	
			$this->html = $this->match('#<div class="text">(.*?)<\/div>#msi', $html, 1);
			$this->matches = $this->match_all('#<p>(.*?)<\/p>#msi', $this->html, 1);
			
			//original ip info
			foreach($this->info_keys as $key => $value){
				$this->ip_info[$value] = $this->replace('#<.*>#', '', $this->matches[$key]);
			}
		}

		return $this->ip_info;
	}
	
	//return the country
	private function get_country(){
		if(isset($this->matches[0])){
			return trim(preg_replace('/<.+>/', '', $a));
		}
	}
	
	
	private function replace($pattern, $with, $string){
		return trim(preg_replace($pattern, $with, $string));
	}
	
	
	/*
	 * set a new ip
	 * */
	function set_ip($ip){
		$this->ip = $ip;
	}
	
	
	//get the current working ip
	function get_ip(){
		return $this->ip;
	}
	
	
	private function get_html(){
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $html = curl_exec($ch);
        curl_close($ch);
        return $html;
	}
	
	
	/*
	 * regex function to regurn everything
	 * */
	private function match($regex, $str, $i = 0) {
    	if(preg_match($regex, $str, $match) == 1)
        	return $match[$i];
        else
        	return false;
    }
	
    
	// for regular expression call
     private function match_all($regex, $str, $i = 0) {
     	if(preg_match_all($regex, $str, $matches) === false)
           return false;
        else
          return $matches[$i];

        }
}

