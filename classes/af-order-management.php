<?php
/*
 * 1. This class creates a tab in order page
 * */

class WooAntiFraudOrderPage{
	
	//initialize
	static function init(){

		//add a metabox in order details page
		add_action( 'add_meta_boxes', array(get_class(), 'woocommerce_meta_boxes' ));
		
		
		//add_action('woocommerce_checkout_order_processed', array(get_class(), 'save_customer_identity'), 10, 2);
		
		
		//before checkout process. Verify if a custom is hard blacklisted or not
		add_action('woocommerce_checkout_process', array(get_class(), 'verify_customer'));
		
		
		//order is created and identify the proxy, vpn, location order meta
		add_action('woocommerce_checkout_update_order_meta', array(get_class(), 'save_customer_identity'), 10, 2);
		
		//add_action('init', array(get_class(), 'check'));
		
		add_action('admin_menu', array(get_class(), 'admin_menu'), 100);
		
		//save stripe info
		add_action('before_processing_stripe_payment', array(get_class(), 'save_customer_stripe_info'), 100, 3);
		
		//for loggedin user
		add_action('save_sripe_info_with_order', array(get_class(), 'save_stripe_response'), 100, 2);
		
		
		register_activation_hook(WOOANTIFRAUD_FILE, array(get_class(), 'plugin_activated'));
		//add_action('init', array(get_class(), 'plugin_activated'));
		
		
		add_action('init', array(get_class(), 'save_customer'));
		
		
		//save fraud lists for evey tab
		add_action('init', array(get_class(), 'save_fraud_lists'), 100);
		
		
		//js
		add_action('admin_enqueue_scripts', array(get_class(), 'admin_enqueue_scripts'));
		
		
		
	}
	
	
	
	/**
	 * admin enqueue scripts
	 * */
	static function admin_enqueue_scripts(){
		
		if($_GET['page'] == 'anti-fraud-management'){		
			wp_register_style('woo_anti_fraud_management_css', WOOANTIFRAUD_URL . 'staticfiles/css/woo_anti_fraud_management_css.css');
			wp_enqueue_style('woo_anti_fraud_management_css');
			
			wp_enqueue_style( 'thickbox' ); // Stylesheet used by Thickbox
   			wp_enqueue_script( 'thickbox' );
   			wp_enqueue_script( 'media-upload' );	
   					
			wp_register_script('woo_anti_fraud_management_js', WOOANTIFRAUD_URL . 'staticfiles/js/woo_anti_fraud_management_js.js', array('jquery'));
			wp_enqueue_script('woo_anti_fraud_management_js');
		}
		
	}
	
	
	
	//admin menu
	static function admin_menu(){
		add_submenu_page('woocommerce', 'Anti Fraud Campaign', 'Frauds', 'manage_woocommerce', 'anti-fraud-management', array(get_class(), 'anti_fraud_management'));
	}
	
	
	/**
	 * Anti Fraud Management
	 */
	static function anti_fraud_management(){
		if(isset($_GET['order_id']) && !empty($_GET['order_id'])){
			include self::get_file_dir('templates/admin/fraud-management.php');
		}
		else{
			include self::get_file_dir('templates/admin/fraud-campaigns.php');
		}
	}
	
	
	/**
	 * convenient way to check things against init hook
	 * */
	static function check(){
		$ip = '180.234.16.208';
		$IP = new WooIpInfo($ip);
		
		$info = $IP->get_ip_info();
		
		var_dump($info);
		exit;
	}
	
	
	//metabox for order page
	static function woocommerce_meta_boxes(){
		add_meta_box( 'woocommerce-order-anit-fraud', __( 'Fraud Order Mangement', 'woocommerce' ), array(get_class(), 'woocommerce_order_fraud_management'), 'shop_order', 'normal', 'high' );
	}
	
	
	//metabox holder
	static function woocommerce_order_fraud_management(){
		include self::get_file_dir('templates/metabox/woocommerce_order_fraud_management.php');
	}
	
	
	static function get_file_dir($file = ''){
		if($file){
			$file = WOOANTIFRAUD_DIR . '/' . $file;
		}
		
		return $file;
	}
	
	
	/*
	 * when an order is created it also saves customer extra identity to be used for anti fraud campaign
	 */
	static function save_customer_identity($order_id, $order_data){
		$ip = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'] ;
				
		$IP = new WooIpInfo($ip);
		$info = $IP->get_ip_info();		
		if($info){
			$info['is_proxy'] = isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ? 1 : 0;
			foreach($info as $key => $value){
				update_post_meta($order_id, '_ip_' . $key, $value);
			}
		}
		
	}
	
	
	
	/*
	 * Save stripe payment info
	 * */
	static function save_customer_stripe_info($order, $stripe_token, $customer_id){
		
		if(is_user_logged_in() && $customer_id){
			$customer_ids = get_user_meta( get_current_user_id(), '_stripe_customer_id', false );
			if ( isset( $customer_ids[ $_POST['stripe_customer_id'] ]['customer_id'] ) ){
				$customer_info = $customer_ids[ $_POST['stripe_customer_id'] ];
				
				if($strip_info = get_post_meta($order->id, '_customer_stripe_info', true)){
					$strip_info[] = $customer_info;
				}
				else{
					$strip_info = array();
					$strip_info[] = $customer_info;
				}
			}
			
			update_post_meta($order->id, '_customer_stripe_info', $strip_info);
		}	
				
	}
	
	
	/*
	 * save the strip info and return the cusomter id
	 * */
	static function save_stripe_info($order, $stripe_token){
		global $woocommerce;
		$available_gateways = $woocommerce->payment_gateways->get_available_payment_gateways();
				
		$data = array(
				'email'       => $order->billing_email,
				'description' => 'Customer: ' . $order->shipping_first_name . ' ' . $order->shipping_last_name,
				'card'        => $stripe_token
			);
		
				
		//var_dump($available_gateways);
			
		$response = $available_gateways[ $woocommerce->checkout->posted['payment_method'] ]->stripe_request($data, 'customers');
		
		
		if($strip_info = get_post_meta($order->id, '_customer_stripe_info', true)){
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		else{
			$strip_info = array();
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		
		update_post_meta($order->id, '_customer_stripe_info',  $strip_info);
		
		return $response->id;
	}
	
	
	
	//this is for logged in users
	static function save_stripe_response($order, $response){
		
		
		if($strip_info = get_post_meta($order->id, '_customer_stripe_info', true)){
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		else{
			$strip_info = array();
			$strip_info[] = array(
				'customer_id' 	=> $response->id,
				'active_card' 	=> $response->active_card->last4,
				'exp_year'		=> $response->active_card->exp_year,
				'exp_month'		=> $response->active_card->exp_month,
			);
		}
		
		update_post_meta($order->id, '_customer_stripe_info',  $strip_info);
	}
	
	
	/**
	 * During plugin activation period
	 * */
	static function plugin_activated(){
		$db = self::get_db_instace();
		return $db->sync_db();
	}
	
	
	/*
	 * database intance
	 * */
	static function get_db_instace(){
		if(!class_exists('WooAntiFraudDb')){
			include self::get_file_dir('classes/class.woo-anti-fraud-db.php');
		}
		
		return new WooAntiFraudDb();
	}
	
	
	
	/**
	 * Save the customer inclugin blcalist parameter
	 * */
	static function save_customer($info){
		if($_POST['customer_profile_save'] == 'Y'){
			$customer_info = array();
			$order = new WC_Order($_POST['order_id']);
			
			$customer_info['user_id'] = 0;
			$customer_info['email'] = $order->billing_email;
			
			if($user = get_user_by('id', $_POST['user_id'])){
				$customer_info['username'] = $user->data->user_login;
				$customer_info['user_id'] = $user->ID;				
			}
			elseif($user = get_user_by('email', $order->billing_email)){
				$customer_info['username'] = $user->data->user_login;
				$customer_info['user_id'] = $user->ID;
			}
		
			$customer_info['blacklist'] = $_POST['blcaklist'];
			$customer_info['attachment'] = $_POST['attachment'];
			$customer_info['note'] = $_POST['customer_additional_note'];
			$customer_info['status'] = $_POST['customer_status'];
			$customer_info['order_id'] = $order->id;
			
			//updateing the database
			$db = self::get_db_instace();
			$customer_id = $db->insert_customer($customer_info);
			
			if($customer_id){
				$db->update_customer_meta($customer_id, 'user_id', $customer_info['user_id']);
				$db->add_order_for_a_customer($customer_id, 'order_id', $customer_info['order_id']);
				$db->update_customer_meta($customer_id, 'note', $customer_info['note']);
				$db->update_customer_meta($customer_id, 'attachment', $customer_info['attachment']);
				
				//saving customer id with this order
				update_post_meta($order->id, '_verified_customer_id', $customer_id);
				
				
				
				//saving the fraud information
				$db->save_detected_frauds($customer_info['blacklist']);
			}
			
			
			$url = admin_url(sprintf('admin.php?page=anti-fraud-management&order_id=%s&message=%s', $order->id, '1'));

			return self::do_redirect($url);
						
		}
	}
	
	
	/**
	 * Save the fraud lists
	 * */
	static function save_fraud_lists(){
		if(isset($_POST['blacklist_by'])){
			return self::update_fraud_lists($_POST['blacklist_by']);
		}
	}
	
	
	/**
	 * Update Fraud lists
	 * */
	static function update_fraud_lists($by){
		$db = self::get_db_instace();
		$lists = $db->get_blacklists_by($by);
		if($lists){
			foreach($lists as $list){
				if(isset($_POST[$by][$list->id])){
					$db->update_blacklist_status('id', $list->id, 1);
				}
				else{
					$db->update_blacklist_status('id', $list->id, 2);
				}
			}
		}
		
	}
	
	
	/**
	 * processing an order
	 * @hook woocommerce_checkout_process
	 * */
	static function verify_customer(){
		global $woocommerce;
		$billing_email = $_POST['billing_email'];
		$db = self::get_db_instace();
		
		if($db->is_black_listed('email', $billing_email)){
			$woocommerce->add_error( '<strong>Sorry! </strong> ' . __( 'We cannot take order from you right now. Please contact with the shop owner', 'woocommerce' ) );
		}
		else{
			$customer = $db->get_customer_by('email', $billing_email);
			if($customer && !empty($customer->username)){
				if($db->is_black_listed('username', $customer->username)){
					$woocommerce->add_error( '<strong>Sorry! </strong> ' . __( 'We cannot take order from you right now. Please contact with the shop owner', 'woocommerce' ) );
				}
			}
		}
					
	}
	
	
	
	/**
	 * do a redirect with the given url
	 * */
	static function do_redirect($url){
		if($url){
			if(!function_exists('wp_redirect')){
				include ABSPATH . '/wp-includes/pluggable.php';
			}
			
			wp_redirect($url);
			exit;
		}
	}
	
	
	
	/**
	 *tab functionality 
	 * */
	static function get_tab_url($tab){
		return sprintf(admin_url('admin.php?page=anti-fraud-management&tab=%s'), $tab['slug']);	
	}
	
	
	
	/**
	 * load the appropriate tab
	 * */
	static function get_appropirate_tab(){
		if(isset($_GET['tab']) && !empty($_GET['tab'])){
			$tab = $_GET['tab'];
		}
		else{
			$tab = 'username';
		}

		return self::get_file_dir('/templates/admin/tabs/' . $tab . '.php');
	}		
	
}