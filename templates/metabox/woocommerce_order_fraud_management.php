<style>
	div.payment_details{
		float: left;
		margin-right: 20px;
			
	}
	
	div.order_data_column_container_antifraud  p{
		color: #777;
	}
	
	div.ip_details{
		float: right;
		margin-right: 100px;		
	}
	
	div.fraud_status{
		font-size: 17px;
		font-wight: bold;
		width: 100%;
		
	}
	
	div.fraud_status p{
		margin-left: 20px;
	}
	
	div.verified{
		border: 1px solid green;
		background: #B8DEB8;
	}
	
	div.not-verified{
		border: 1px solid green;
		background: #BFBFBF;		
	}
	
	div.fraud{
		border: 1px solid #FF0000;
		background: #FCA7A7;		
	}
	
</style>


<?php 
	global $post, $wpdb, $thepostid, $theorder, $order_status, $woocommerce;
	$thepostid = absint( $post->ID );

	if ( ! is_object( $theorder ) )
		$theorder = new WC_Order( $thepostid );

	$order = $theorder;
	
	$available_gateways = $woocommerce->payment_gateways->get_available_payment_gateways();
	//var_dump($available_gateways);
	
?>
<div class="woocommerce farud-order-management">
	
	<?php 
		
		//identifing the customer of this specific order.
		
		$db = self::get_db_instace();
		$customer = $db->get_customer_by('email', $order->billing_email);
		
	//	var_dump($customer);
		
		if($customer){
			$is_verified = true;
			if($db->is_black_listed('email', $customer->email)){
				$is_blacklisted = true;
			}
			elseif(!empty($customer->username) && $db->is_black_listed('username', $customer->username)){
				$is_blacklisted = true;
			}
			else{
				$is_blacklisted = false;
			}
		}
		else{
			$is_verified = false;
		}
	?>
	
	<?php 
		if($is_blacklisted && $is_verified){
			?>
				<div class="fraud_status fraud">
					<p> Blacklisted!!! </p>
				</div>
			<?php 
					
		}
		elseif (!$is_blacklisted && $is_verified){
			?>
				<div class="fraud_status verified">
					<p>Verified</p>
				</div>			
			<?php 
		}
		elseif(!$is_verified){
			?>
				<div class="fraud_status not-verified">
					<p> <a href="<?php echo admin_url('admin.php?page=anti-fraud-management&order_id=' . $order->id); ?>"> Please verify the customer</a> </p>
				</div>			
			<?php 			
		}
			
	?>
	
			
		<div class="order_data_column_container_antifraud">
			
		<?php 
			if($order->payment_method == 'paypal'){
				$payer_name = get_post_meta($post->ID, 'Payer first name', true) . ' ' . get_post_meta($post->ID, 'Payer last name', true);
				$billing_name = $order->billing_first_name . ' ' . $order->billing_last_name;
			
				$payer_email = get_post_meta($post->ID, 'Payer PayPal address', true);
			?>
				
				<div class="payment_details">
					<h4>Payment: <?php echo $available_gateways[$order->payment_method]->title; ?></h4>
					<p><strong>Paypal Name: </strong><?php echo $payer_name;	?></p>
					<p><strong>Paypal Email: </strong><?php echo $payer_email; ?></p>
					<p>
						<strong>Status (Billing address) :</strong><br/>
						
						<?php echo strtolower($payer_name) == strtolower($billing_name) ? 'Matched' : 'Not Matched'; ?>
											
					</p>
					
					<p>
						<strong>Status (Paypal Email) :</strong><br/>
						
						<?php echo strtolower($payer_email) == strtolower($order->billing_email) ? 'Matched' : 'Not Matched' ?>
											
					</p>
										
				</div>
				
				<?php 
					}
					elseif($order->payment_method == 'stripe'){
						$stripe_info = get_post_meta($order->id, '_customer_stripe_info', true);
						?>
						
						<div class="payment_details">
						<h4>Payment: <?php echo $available_gateways[$order->payment_method]->title; ?></h4>
						<p> Attempted Transaction: <?php echo count($stripe_info); ?> </p>
						<?php 
							if($stripe_info){
								
							//	var_dump($stripe_info);
								
								foreach($stripe_info as $si){
									?>
									
										<h4>Card Info</h4>
										<p><strong> Card Number: </strong> .... <?php echo $si['active_card']; ?> </p>
										<p><strong> Exp Year : </strong> <?php echo $si['exp_year']; ?> </p>
										<p><strong> Exp Month : </strong> <?php echo $si['exp_month']; ?> </p> 
									
									
									<?php 
								}
							}
						?>
						
					</div>
					
						
						<?php 						
					}
					else{
						?>
						<div class="payment_details">
							<h4>Payment: <?php echo $available_gateways[$order->payment_method]->title; ?></h4>
						</div>
						<?php 
					}

				?>
				
				<div class="ip_details">
					<h4>IP Details</h4>
					<p>
						<strong>IP: </strong><?php echo get_post_meta($order->id, '_customer_ip_address', true); ?>
					</p>
					<p>
						<strong>Country: </strong>  <?php echo get_post_meta($order->id, '_ip_country', true); ?>
					</p>
					<p>
						<strong>State: </strong>  <?php echo get_post_meta($order->id, '_ip_state', true); ?>
					</p>
					<p>
						<strong>City: </strong>  <?php echo get_post_meta($order->id, '_ip_city', true); ?>
					</p>
					<p>
						<strong>ISP: </strong>  <?php echo get_post_meta($order->id, '_ip_isp', true); ?>
					</p>
					<p>
						<strong>Timezone: </strong>  <?php echo get_post_meta($order->id, '_ip_time_zone', true); ?>
					</p>
					
					<p>
						<strong>Proxy Used?: </strong>  <?php echo (get_post_meta($order->id, '_ip_is_proxy', true)) ? 'Yes' : 'No'; ?>
					</p>
					
				</div>
									
				<div style="clear:both"></div>
			
			</div>
			
	</div>