<?php 
	$db = self::get_db_instace();
	$emails = $db->get_blacklists_by('email');
	//var_dump($emails);
	$per_row = 5;
?>

<?php 
	if(isset($_POST['blacklist_by'])){
		echo '<div class="updated"><p>Saved</p></div>';
	}
?>

<div class="tab email">
<form action="" method="post">

	<input type="hidden" name="blacklist_by" value="email" />

	<p class="instruction">Emails of the customers. Unlike username email also exists if a customer is  user of the blog. </p>
	
	<?php 
		if($emails){
		?>
			<table class="form-table">
			<?php 
				foreach($emails as $key => $email){
					if(fmod($key, $per_row) == 0 || $key == 0) echo '<tr>';
						?>
						<td class="<?php echo $email->status == 1 ? 'blacklisted' : ''; ?>"> <input <?php checked('1', $email->status); ?> id="<?php echo $email->value . '_' . $email->id; ?>" type="checkbox" name="<?php echo $email->type . '[' . $email->id . ']'; ?>" value="1" /> <label for="<?php echo $email->value . '_' . $email->id; ?>"><?php echo $email->value; ?></label> </td>
						<?php 
					if((fmod($key, $per_row) == 0 && $key > 0) || $key == count($emails) - 1) echo '</tr>';
				}		
			?>
			</table>
		<?php 				
		}
	?>

	
	<p><input class="button button-primary" type="submit" value="Save"></p>
</form>
</div>