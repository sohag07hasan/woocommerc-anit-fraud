<?php 
	$db = self::get_db_instace();
	$ips = $db->get_blacklists_by('ip');
	//var_dump($ips);
	$per_row = 5;
?>

<?php 
	if(isset($_POST['blacklist_by'])){
		echo '<div class="updated"><p>Saved</p></div>';
	}
?>

<div class="tab ip">
<form action="" method="post">

	<input type="hidden" name="blacklist_by" value="ip" />

	<p class="instruction">Ip Address of the customers </p>
	
	
	<?php 
		if($ips){
		?>
			<table class="form-table">
			<?php 
				foreach($ips as $key => $ip){
					if(fmod($key, $per_row) == 0 || $key == 0) echo '<tr>';
						?>
						<td class="<?php echo $ip->status == 1 ? 'blacklisted' : ''; ?>"> <input <?php checked('1', $ip->status); ?> id="<?php echo $ip->value . '_' . $ip->id; ?>" type="checkbox" name="<?php echo $ip->type . '[' . $ip->id . ']'; ?>" value="1" /> <label for="<?php echo $ip->value . '_' . $ip->id; ?>"><?php echo $ip->value; ?></label> </td>
						<?php 
					if((fmod($key, $per_row) == 0 && $key > 0) || $key == count($ips) - 1) echo '</tr>';
				}		
			?>
			</table>
		<?php 				
		}
	?>

	
	<p><input class="button button-primary" type="submit" value="Save"></p>
</form>
</div>