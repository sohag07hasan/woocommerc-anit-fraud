<?php 
	$db = self::get_db_instace();
	$cities = $db->get_blacklists_by('city');
	//var_dump($cities);
	$per_row = 8;
?>

<?php 
	if(isset($_POST['blacklist_by'])){
		echo '<div class="updated"><p>Saved</p></div>';
	}
?>

<div class="tab city">
<form action="" method="post">

	<input type="hidden" name="blacklist_by" value="city" />

	<p class="instruction">Cities of the customers </p>
	
	<?php 
		if($cities){
		?>
			<table class="form-table">
			<?php 
				foreach($cities as $key => $city){
					if(fmod($key, $per_row) == 0 || $key == 0) echo '<tr>';
						?>
						<td class="<?php echo $city->status == 1 ? 'blacklisted' : ''; ?>"> <input <?php checked('1', $city->status); ?> id="<?php echo $city->value . '_' . $city->id; ?>" type="checkbox" name="<?php echo $city->type . '[' . $city->id . ']'; ?>" value="1" /> <label for="<?php echo $city->value . '_' . $city->id; ?>"><?php echo $city->value; ?></label> </td>
						<?php 
					if((fmod($key, $per_row) == 0 && $key > 0) || $key == count($cities) - 1) echo '</tr>';
				}		
			?>
			</table>
		<?php 				
		}
	?>

	
	<p><input class="button button-primary" type="submit" value="Save"></p>
</form>
</div>