<?php 
	$db = self::get_db_instace();
	$usernames = $db->get_blacklists_by('username');
	//var_dump($usernames);
	$per_row = 8;
?>

<?php 
	if(isset($_POST['blacklist_by'])){
		echo '<div class="updated"><p>Saved</p></div>';
	}
?>

<div class="tab username">
<form action="" method="post">

	<input type="hidden" name="blacklist_by" value="username" />
	<input type="hidden" name="selected_tab" value="username">	
	
	<p class="instruction">Usernames of the customers. Username only exists if a customer is an user of the blog. </p>
	
	
	<?php 
		if($usernames){
		?>
			<table class="form-table">
			<?php 
				foreach($usernames as $key => $username){
					if(fmod($key, $per_row) == 0 || $key == 0) echo '<tr>';
						?>
						<td class="<?php echo $username->status == 1 ? 'blacklisted' : ''; ?>"> <input <?php checked('1', $username->status); ?> id="<?php echo $username->value . '_' . $username->id; ?>" type="checkbox" name="<?php echo $username->type . '[' . $username->id . ']'; ?>" value="1" /> <label for="<?php echo $username->value . '_' . $username->id; ?>"><?php echo $username->value; ?></label> </td>
						<?php 
					if((fmod($key, $per_row) == 0 && $key > 0) || $key == count($usernames) - 1) echo '</tr>';
				}		
			?>
			</table>
		<?php 				
		}
	?>

	
	<p><input class="button button-primary" type="submit" value="Save"></p>
</form>
</div>