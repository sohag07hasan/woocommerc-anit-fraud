<?php 
	$db = self::get_db_instace();
	$countries = $db->get_blacklists_by('country');
	//var_dump($countries);
	$per_row = 5;
?>

<?php 
	if(isset($_POST['blacklist_by'])){
		echo '<div class="updated"><p>Saved</p></div>';
	}
?>

<div class="tab country">
<form action="" method="post">

	<input type="hidden" name="blacklist_by" value="country" />

	<p class="instruction">Countries of the customers </p>
	
	<?php 
		if($countries){
		?>
			<table class="form-table">
			<?php 
				foreach($countries as $key => $country){
					if(fmod($key, $per_row) == 0 || $key == 0) echo '<tr>';
						?>
						<td class="<?php echo $country->status == 1 ? 'blacklisted' : ''; ?>"> <input <?php checked('1', $country->status); ?> id="<?php echo $country->value . '_' . $country->id; ?>" type="checkbox" name="<?php echo $country->type . '[' . $country->id . ']'; ?>" value="1" /> <label for="<?php echo $country->value . '_' . $country->id; ?>"><?php echo $country->value; ?></label> </td>
						<?php 
					if((fmod($key, $per_row) == 0 && $key > 0) || $key == count($countries) - 1) echo '</tr>';
				}		
			?>
			</table>
		<?php 				
		}
	?>

	
	<p><input class="button button-primary" type="submit" value="Save"></p>
</form>
</div>