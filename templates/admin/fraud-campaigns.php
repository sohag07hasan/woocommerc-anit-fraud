<style>
	.instruction{
		font-size: 15px;
	}
	
	.blacklist{
		color: red;
	}
	
	td.blacklisted{
		font-weight: bold;
	}
	
	div.tab form td{
		font-size: 15px;
	}
	
</style>

<?php 
	
	$page_url = admin_url('admin.php?page=anti-fraud-management');
	$tabs = array(
		array(
			'slug' => 'username',
			'display' => 'Username'
		),	
		array(
			'slug' => 'email',
			'display' => 'Email'
		),
		array(
			'slug' => 'ip',
			'display' => 'IP Address'
		),
		array(
			'slug' => 'city',
			'display' => 'City'
		),
		array(
			'slug' => 'country',
			'display' => 'Country'
		),
	/*		
		array(
			'slug' => 'other',
			'display' => 'Other'
		),
	*/	
	);
	
?>

<div class="wrap">
	<?php screen_icon('tools'); ?>
	<h2 class="nav-tab-wrapper">
		<?php 
			foreach($tabs as $tab){
				if(isset($_GET['tab']) && $_GET['tab'] == $tab['slug']){
					$class = 'nav-tab-active nav-tab';
				}
				elseif(empty($_GET['tab']) && $tab['slug'] == 'username'){
					$class = 'nav-tab-active nav-tab';
				}
				else{
					$class = 'nav-tab';
				}
				?>
				<a class="<?php echo $class; ?>" href="<?php echo self::get_tab_url($tab); ?>"><?php echo $tab['display']; ?></a>
				<?php 
			}
		?>
	</h2>
	
	<?php 
		include self::get_appropirate_tab();
	?>
	
</div>