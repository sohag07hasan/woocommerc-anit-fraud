<?php 
	if(!isset($_GET['order_id'])) return;
	
	$order = new WC_Order( $_GET['order_id'] );
	
	$user_id = get_post_meta($order->id, '_customer_user', true);
	$user = get_user_by('id', $user_id);
	$customer_id =	get_post_meta($order->id, '_verified_customer_id', true);
	
	$db = self::get_db_instace(); // $db = new WooAntiFraudDb();
	$customer = $db->get_customer_by('ID', $customer_id);
	
	//var_dump($customer);
	
	//var_dump($db->is_black_listed('username', $user->data->user_login));
	
?>

<style>
	tr.odd{
		background-color: #BFBFBF;
	}
</style>

<div class="wrap">
	<h2>Order Number #<?php echo $order->id ?></h2>
	
	<?php 
		if($_GET['message'] == 1){
			echo '<div class="updated"><p>Saved </p></div>';
		}
	?>
	
	<form action="<?php echo admin_url(sprintf('admin.php?page=anti-fraud-management&order_id=%s', $order->id)); ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="customer_profile_save" value="Y" />
		<input type="hidden" name="order_id" value="<?php echo $order->id; ?>" />
		<input type="hidden" name="user_id" value="<?php echo $user->ID; ?>" />
		
		<h4>Customer's Basic Information</h4>		
		<table class="form-table">
			<tr class="odd">
				<th scope="row">User Name</th>
				<td><?php echo (empty($user)) ? 'Non User' : $user->data->user_login; ?></td>
				<td> 
					<?php if($user) : ?>
						<input <?php echo $db->is_black_listed('username', $user->data->user_login) ? 'checked' : ''; ?> id="username" type="checkbox" name="blcaklist[username]" value="<?php echo $user->data->user_login; ?>" /> <label for="username">Blcklisted</label> 
					<?php else: echo '&nbsp;'; endif; ?>
				</td>
			</tr>
			
			<tr>
				<th scope="row">Email </th>
				<td><?php echo $order->billing_email; ?></td>
				<td> <input <?php echo $db->is_black_listed('email', $order->billing_email) ? 'checked' : ''; ?>  id="email" type="checkbox" name="blcaklist[email]" value="<?php echo $order->billing_email; ?>" /> <label for="email"> Blcklisted</label> </td>
			</tr>
			
			<tr class="odd">
				<th scope="row">Payment </th>
				<td>
				<?php 
					if($order->payment_method == 'paypal'){
						echo 'Paypal';
					}
					elseif($order->payment_method == 'stripe'){
						echo 'Stripe';
					}
				?>
				</td>
				<td> &nbsp; </td>
			</tr>
			
			<tr>
				<th scope="row">Payment ID</th>
				<td><?php 
					if($order->payment_method == 'paypal'){
						echo get_post_meta($order->id, 'Payer PayPal address', true);
					}
					elseif($order->payment_method == 'stripe'){
						$stripe_info = get_post_meta($order->id, '_customer_stripe_info', true);
						$card_info = array();
						
						if(is_array($stripe_info)){
							foreach($stripe_info as $si){
								$card_info[] = $si['active_card'];						
							}
							echo implode(', ', $card_info);
						}
					}
				?></td>
				<td> &nbsp; </td>
			</tr>
				
			<tr class="odd">
				<th scope="row">Customer IP</th>
				<td><?php 
					$ip = get_post_meta($order->id, '_customer_ip_address', true);
					echo $ip;
				?></td>
				
				<td> <input <?php echo $db->is_black_listed('ip', $ip) ? 'checked' : ''; ?> id="ip" type="checkbox" name="blcaklist[ip]" value="<?php echo $ip; ?>" /> <label for="ip">Blcklisted </label></td>
				
			</tr>
			
			<tr>
				<th scope="row">Customer City (traced by IP)</th>
				<td><?php 
					$city = get_post_meta($order->id, '_ip_city', true);
					echo $city;
				?></td>
				
				<td> <input <?php echo $db->is_black_listed('city', $city) ? 'checked' : ''; ?> id="city" type="checkbox" name="blcaklist[city]" value="<?php echo $city; ?>" /> <label for="city">Blcklisted</label> </td>
			</tr>
			
			<tr class="odd">
				<th scope="row">Customer Country (traced by IP)</th>
				<td><?php 
					$country = get_post_meta($order->id, '_ip_country', true);
					echo $country;
				?></td>
				
				<td> <input <?php echo $db->is_black_listed('country', $country) ? 'checked' : ''; ?> id="country" type="checkbox" name="blcaklist[country]" value="<?php echo $country; ?>" /> <label for="country">Blcklisted</label> </td>
				
			</tr>
			
			<tr>
				<th scope="row">Customer Timezone (traced by IP)</th>
				<td><?php 
					echo get_post_meta($order->id, '_ip_time_zone', true);
				?></td>
				
				<td> &nbsp; </td>
			</tr>
			
		</table>	
	
		
		<h4>Additional Information</h4>	
		
		<?php 
			if(!empty($customer)){
				$note = $db->get_customer_meta($customer->ID, 'note');
				$attachment = $db->get_customer_meta($customer->ID, 'attachment');
				$status = $customer->status;
			}
		?>
		
			
		<table class="form-table">
			<tr>
				<th scope="row">Attach Image/Document</th>
				<td>
								
					
					<span class='upload'>
				        <input type='text' id='wptuts_favicon' class='regular-text text-upload' name='attachment' value='<?php echo esc_url( $attachment ); ?>'/>
				        <input type='button' class='button button-upload' value='Upload Attachment/Image'/></br>
				        
				        <?php if($attachment): ?>
				        	<a class="preview-upload" href="<?php echo $attachment; ?>" target="_blank">View</a>
				        <?php endif; ?>
				        
				     </span>
					 
					<div id="customer_attachment_link_puller" style="display: none"></div>
				</td>
			</tr>
			
			<tr>
				<th scope="row">Additional Note</th>
				<td>					
					<textarea rows="5" cols="100" name="customer_additional_note"><?php echo $note; ?></textarea>
				</td>
			</tr>
			
			<!-- 
				<tr>
					<th scope="row"> Customer Status </th>
					<td>
						<select name="customer_status">
							<option <?php selected(1, $status); ?> value="1"> Whitelist </option>
							<option <?php selected(2, $status); ?> value="2"> Soft Blacklist </option>
							<option <?php selected(3, $status); ?> value="3"> Hard Blacklist </option>
						</select>
					</td>
				</tr>
			 --> 
		</table>
		
		<p><input type="submit" value="Save Customer" class="button button-primary" /></p>
		
	</form>		
	
</div>