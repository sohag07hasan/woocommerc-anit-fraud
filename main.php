<?php 
/*
 * plugin name: Woocommerce Anti Fraud Campaign
 * author: Mahibl Hasan SOhag
 * Description: It helps the shop owner to manage fraud orders and block them from future order
 * 
 * */

define("WOOANTIFRAUD_FILE", __FILE__);
define("WOOANTIFRAUD_DIR", dirname(__FILE__));
define("WOOANTIFRAUD_URL", plugins_url('/', __FILE__));




include WOOANTIFRAUD_DIR . '/classes/af-order-management.php';
WooAntiFraudOrderPage::init();

include WOOANTIFRAUD_DIR . '/classes/class.ipaddress-info-parser.php';
